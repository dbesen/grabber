<?
	// Emulate register_globals on
	if (!ini_get('register_globals')) {
		$superglobals = array($_SERVER, $_ENV,
			$_FILES, $_COOKIE, $_POST, $_GET);
		if (isset($_SESSION)) {
			array_unshift($superglobals, $_SESSION);
		}
		foreach ($superglobals as $superglobal) {
			extract($superglobal, EXTR_SKIP);
		}
	}

	ini_set('memory_limit', '128M');
	date_default_timezone_set("America/Denver");
	require_once("inc/auth.php");
	require_once("time.php");
	starttimer();
	set_time_limit(60 * 5);

	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("Content-Type: text/html; charset=\"UTF-8\"");

	// Refresh the ignores cookie
	if(isset($_COOKIE['grabber_ignores'])) {
		setcookie('grabber_ignores', "1", time()+60*60*24*30);
	}
	error_reporting(E_ALL);

	require_once("regex.php");
	require_once("irc.php");

//	ob_start("ob_gzhandler");

	$home = $_SERVER['DOCUMENT_ROOT'];
	$LineIgnoresPath = "$home/ignores/line-ignores.txt";
	$UrlIgnoresPath = "$home/ignores/url-ignores.txt";
	$WordIgnoresPath = "$home/ignores/word-ignores.txt";

	// ------------- settings
	$LogPath = "/home/besen/log/";
	$MaxLines = 8000;
	// -----------------

	$log = new IrcLog;

	// $t is no longer used.
	if(isset($t)) {
		header("Location: /");
		exit();
	}

	// 168px w/o url link on left, 196 with

	?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><?
	?><html><title>Urlgrabber</title><head><?
/*	?><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><? // this crashes IE (?) */
	?><style><!--
.i{font-family:Courier New, monospace; font-size: 12px; line-height: 14.96px; display: block; text-indent: -168px; margin-left: 168px; word-wrap: break-word; }
.m{font-family:Courier New, monospace; font-size: 12px; }
//-->
</style>
<script language="javascript">
function a(u) {
	if(typeof event == 'undefined') return true;
	if(event.button != 1) return true;
        if(u.indexOf('/') == -1) {
                location.href = unescape(u);
        } else {
                location.href = u;
        }
	return false;
}

function f() {
	if(typeof event == 'undefined') return true;
	if(navigator.appName == "Microsoft Internet Explorer") return false;
	return true;
}

function g(event, what, url) {
	if (event.which == null)
		/* IE case */
		button= (event.button < 2) ? "LEFT" :
			((event.button == 4) ? "MIDDLE" : "RIGHT");
	else
		/* All others */
		button= (event.which < 2) ? "LEFT" :
			((event.which == 2) ? "MIDDLE" : "RIGHT");

	if(button != "RIGHT") {
		what.href='strip-referrer.php?u=' + url
	}
		
}

function gb(what, url) {
	what.href = url;
}

</script>
<link rel="alternate" type="application/rss+xml" href="http://guantes.f3h.com/paste/grabrss.php" title="Urlgrabber RSS Feed">
</head><body bgcolor="#FFFFFF"><form style="margin:0"><?
	?><span class="m">sargon's <a href="/">Urlgrabber</a>&nbsp;|&nbsp;<?

	if(!isset($channel)) $channel = "#hatcave";
	if(!isset($search)) $search = null;
	$search = stripslashes($search);
	if(!isset($justurls)) $justurls = "Just Urls";
	if(!isset($dr)) $dr = 1;
	if(!isset($day)) $day = date("d");
	if(!isset($month)) $month = date("m");
	if(!isset($year)) $year = date("Y");
	if(isset($_COOKIE['grabber_ignores'])) $ignores = false;
	else $ignores = true;
	$p = 0;
	if(isset($plain)) $p = 1;

	$internet_explorer = false; $opera = false;
	if(strstr($GLOBALS['HTTP_USER_AGENT'], "MSIE")) $internet_explorer = true;
	if(strstr($GLOBALS['HTTP_USER_AGENT'], "Opera")) { $internet_explorer = false; $opera = true; }

//	if(strlen($search) > 0) $dr = 2;

	print_select("justurls", array("Just Urls", "Full Text"));
	?>&nbsp;|&nbsp;<?
	print "<input type=\"radio\" name=\"dr\" value=\"1\""; if($dr == 1) print " checked"; print ">";
	print "date: "; print_select("month", get_months());
	print "/"; print_select("day", get_days());
	print "/"; print_select("year", get_years());
	print " <input type=\"radio\" name=\"dr\" value=\"2\""; if($dr == 2) print " checked"; print ">Any";
	?>&nbsp;|&nbsp;<?
	print "chan: "; print_select("channel", $log->get_channels());
	?>&nbsp;|&nbsp;<?
	print "search: "; print "<input type=\"text\" name=\"search\" value=\"" . htmlentities($search) . "\" size=\"25\" class=\"m\">";
	?>&nbsp;|&nbsp;<?
	?><input type="submit" value="go" class="m"></span></form><hr size="1"><?

	if($justurls == "Just Urls") $justurls = true; else $justurls = false;
	if($dr == 2) $date = "*";
	else $date = "$month.$day.$year";
	$log->setMaxLines($MaxLines);
	$content = $log->get($channel, $search, $date, $justurls, $ignores);

	// if count is < 10
	if($date != "*" && count($content) < 20 && strlen($search) == 0 && $justurls == "Just Urls") {
		// calculate yesterday's date
		$yesterday = mktime(0, 0, 0, $month, $day - 1, $year);
		$p_month = date("m", $yesterday);
		$p_day = date("d", $yesterday);
		$p_year = date("Y", $yesterday);
		$p_date = "$p_month.$p_day.$p_year";
		// append to content
		$content = array_merge($content, $log->get($channel, $search, $p_date, $justurls, $ignores));
	}

	$processed = getelapsed();
	starttimer();
	foreach($content as $item) {
		$line = $item['line'];
		print "<div class=\"i\">";
		/* uncomment me for url link on left
		unset($Url);
		if(is_array($item['urls']))
			$Url = array_pop($item['urls']);
		if(isset($Url)) print "<a href=\"$Url\">url</a> ";
		else print "&nbsp;&nbsp;&nbsp;&nbsp;";
		*/
		$line = anchor_date($line);
		print chomp($line) . "</div>\n";
	}

	print "<hr size=\"1\"><span class=\"m\">Total: " . $log->count;
	$elapsed = getelapsed();
        print "&nbsp;|&nbsp;gen {$processed}s, xfer {$elapsed}s";
	?>&nbsp;|&nbsp;<a href="/golast.php">golast</a> (<a href="/golast.php?channel=<?=urlencode($channel)?>">for this chan</a>)<?
	?>&nbsp;|&nbsp;<a href="/quotes">quotes</a><?
	?>&nbsp;|&nbsp;simple <a href="/stats">stats</a><?
	?>&nbsp;|&nbsp;<a href="/ignores/">ignores</a> are <a href="/toggle_ignores.php?<?=$GLOBALS['QUERY_STRING']?>"><?
	if($ignores == true) print "on"; else print "off";
	?></a> (<?=$ignorecount?> ignored)<?
	?>&nbsp;|&nbsp;<form style="margin: 0; display: inline;" name='logoutForm' method='post'><input type='hidden' name='logout' value='1' /><a href='javascript:void(0);' onclick='logoutForm.submit(); return false;'>Logout</a></form><?
	print "</span>";
 


	?></body></html><?

	function chomp($str) {
		$str = preg_replace("/\n$/", "", $str);
		return $str;
	}

	function print_select($name, $options) {
		global $$name;
		// gets default from environment
		print "<select name=\"$name\" class=\"m\">";
		foreach($options as $option) {
			print "<option";
			if($$name == $option) print " selected";
			print ">$option</option>\n";
		}
		print "</select>";
	}

	function get_months() {
		$ret = array("*");
		for($i="1";$i<="12";$i++) $ret[] = sprintf("%02d", $i);
		return $ret;
	}
	function get_days() {
		$ret = array("*");
		for($i=1;$i<=31;$i++) $ret[] = sprintf("%02d", $i);
		return $ret;
	}
	function get_years() {
		$ret = array("*");
		$year = date("Y");
		for($i=2000;$i<=$year;$i++) $ret[] = $i;
		return $ret;
	}

	function anchor_date($line) {
		global $search;
		global $channel;
		global $justurls;


		// Split out the date
		if(!preg_match("/\[([^\]]+)\] (#[^ ]*)?/", $line, $matches)) return $line;
		$d = $matches[1];
		if(isset($matches[2])) $l_chan = strtolower($matches[2]);
		else $l_chan = "";

		// to save space, instead of urlencoding we make it numeric; also, it doesn't need the date
		//$ud = urlencode($d);

		if(!preg_match('/(\d\d)\/(\d\d)\/(\d\d) (\d\d):(\d\d):(\d\d)/', $d, $matches)) return $line;

		$m = $matches[1];
		$day = $matches[2];
		$year = "20" . $matches[3];

		$ud = $matches[4] . $matches[5] . $matches[6];

		// only print this if we searched..
		$c = urlencode(strtolower($channel));
		if($l_chan != "") $c = urlencode($l_chan); // go to specific channel if possible
		$href = " href=\"?justurls=Full+Text&channel=$c&month=$m&day=$day&year=$year#$ud\"";
//		$href = " style=\"color: black;\" href=\"?justurls=Full+Text&channel=$c&month=$m&day=$day&year=$year#$ud\"";

//		if(($search == null) && ($justurls == false)) $href = "";

		// Make it an anchor
		$ad = "<a name=\"$ud\"$href>$d</a>";

		// Replace it back in
		return str_replace($d, $ad, $line);
	}
?>
