<?
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

	include("/home/sargon/xem/random/jpgraph/src/jpgraph.php");
	include("/home/sargon/xem/random/jpgraph/src/jpgraph_line.php");
	include("/home/sargon/xem/random/jpgraph/src/jpgraph_date.php");
	include("/home/sargon/xem/random/jpgraph/src/jpgraph_utils.inc.php");
	// Number of lines per day by channel or overall
	
	// todo: does the output graph need to be cached?
	// todo: set min since # is weird
	// todo: why does linux have the /\ things?

	putenv("LANG=C"); // speeds up wc
	chdir("/home/sargon/log");
	if(!isset($chan)) $chan = "#hatcave";
	if(!isset($zoom)) $zoom = 0;
	$data = array();
	$xdata = array();

	$echan = escapeshellarg($chan);
	$wc = `wc -l $echan/*.log | grep -v total`;
	$wc = explode("\n", $wc);
	foreach($wc as $line) {
		$line = trim($line);
		if($line == "") continue;
		$parts = explode(" ", $line);
		$count = $parts[0];
		$parts = explode('/', $parts[1]);
		$day = $parts[1];

		$time = mktime(0, 0, 0, substr($day, 0, 2), substr($day, 3, 2), substr($day, 6, 4));
		if($count > 0) $data[$time] = $count;
	}
	ksort($data);
	$xdata = array_keys($data);
	$data = array_values($data);

	$graph = new Graph(800, 400);
	if($zoom) $graph->title->Set($chan . " (zoomed)");
	else $graph->title->Set($chan);
	if($zoom) $graph->SetScale('intlin');
	else {
		$start_time = mktime(0, 0, 0, 10, 25, 2000);
		$end_time = time();
		$graph->SetScale('intlin',0,8000,$start_time,$end_time);
	}
	$graph->SetMarginColor('white');
	$graph->SetMargin(40,30,40,80);
	$graph->xaxis->setTickSide(SIDE_BOTTOM);
	$graph->yaxis->SetTickSide(SIDE_LEFT);
	$graph->xaxis->SetPos('min');
//	$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,9);

	if($zoom) list($tickPositions, $minTickPositions) = DateScaleUtils::GetTicks($xdata);
	else list($tickPositions, $minTickPositions) = DateScaleUtils::GetTicksFromMinMax($start_time, $end_time,1);
	if(count($tickPositions) > 2) {
		$graph->xaxis->SetTickPositions($tickPositions, $minTickPositions);
		$graph->xaxis->SetLabelFormatString('M, Y',true);
	}
	else $graph->xaxis->SetLabelFormatString('M d, Y',true);

//	$graph->xaxis->SetTickLabels($xdata);
	if(!$zoom) $graph->xaxis->setTextLabelInterval(2);
//	$graph->xaxis->setTextTickInterval(5);

	$p = new LinePlot($data,$xdata);
	if($zoom) $p->SetFillColor('#DD7866');
	else $p->SetFillColor('#7AAED6');

	$graph->Add($p);
	$graph->xaxis->SetLabelAngle(90); // todo: set this to 30 (requires ttf font)
	$graph->Stroke();

?>
