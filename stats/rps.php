<?
	$a = gettimeofday();
	$starttime = $a[sec]+($a[usec]/1000000);

	Header("Content-type: text/plain");
	print "Roshambot stats:\n";
	$handle = popen("cat /home/sargon/log/#*/*.log", "r");
	while(!feof($handle)) {
		$line = fgets($handle);
		if(substr($line, 25, 9) != "roshambot") continue;
		if(preg_match_all("/([^ ]+)'/", $line, $matches)) {
			$loser = substr($matches[1][0], 0, 3);
			$winner = substr($matches[1][1], 0, 3);
			$loser = trim(str_replace("-", " ", $loser));
			$winner = trim(str_replace("-", " ", $winner));
			$wins[$winner]['win'][$loser]++;
			$wins[$loser]['loss'][$winner]++;
		}
	}
	function compare($object1, $object2) {
		$winsum = 0;
		if(isset($object1['win'])) $winsum = array_sum($object1['win']);
		$losssum = 0;
		if(isset($object1['loss'])) $losssum = array_sum($object1['loss']);

		$winratio1 = $winsum / ($winsum + $losssum);
		$winratio1 *= 100;
		if($losssum == 0) $winratio1 = -1;

		$winsum = 0;
		if(isset($object2['win'])) $winsum = array_sum($object2['win']);
		$losssum = 0;
		if(isset($object2['loss'])) $losssum = array_sum($object2['loss']);

		$winratio2 = $winsum / ($winsum + $losssum);
		$winratio2 *= 100;
		if($losssum == 0) $winratio2 = -1;

		if($winratio1 < $winratio2) return 1;
		if($winratio1 > $winratio2) return -1;
		return 0;
	}
	uasort($wins, "compare");
	//print_r($wins);
	foreach($wins as $person=>$values) {
		$winsum = 0;
		if(isset($values['win'])) $winsum = array_sum($values['win']);
		$losssum = 0;
		if(isset($values['loss'])) $losssum = array_sum($values['loss']);

		$winratio = $winsum / ($winsum + $losssum);
		$winratio *= 100;
		print "\nNick $person: ";
		print "(" . $winsum . " wins, " . $losssum . " losses: $winratio%)\n";
		print " wins: ";
		if(!isset($values['win'])) {
			print "none\n";
		} else {
			print "\n";
			foreach($values['win'] as $nick=>$win) {
				print "    beat $nick $win times\n";
			}
		}
		print " losses: ";
		if(!isset($values['loss'])) {
			print "none\n";
		} else {
			print "\n";
			foreach($values['loss'] as $nick=>$loss) {
				print "    lost to $nick $loss times\n";
			}
		}
	}
	$a = gettimeofday();
	$endtime = $a[sec]+$a[usec]/1000000;
	print "\n\nGenerated in ";
	print $endtime-$starttime . " sec\n";
?>
