<?
	if(!isset($s)) {
		print "Normalized stats for #hatcave:<br>\n";
		print "<form><input name=\"s\"><input type=\"submit\" value=\"Regex Search\"><br>\n";
		print "<input type=\"checkbox\" name=\"f\" value=\"1\" checked> Ignore nick, channel, timestamp, etc (prepend '(>|\* ).*' to regex)\n";
		print "</form>\n";
		print "<a href=\"http://xem.us/g/stats/stats2.php?s=&f=1\">everything</a><br>";
		exit();
	}
	if($f == 1)
		$s = "(>|\* ).*" . stripslashes($s);
	else
		$s = stripslashes($s);
	$a = gettimeofday();
	$starttime = $a[sec]+($a[usec]/1000000);

	Header("Content-type: text/plain");
	print "Regex is $s\n";
	set_time_limit(60 * 5); // 5 mins
	print "Calculating... This could take several minutes\n"; flush();
//	$handle = popen("egrep -i -h $s /home/sargon/log/#*/*.log", "r");
//	$handle = popen("cat /home/sargon/log/#*/*.log", "r"); // owwwwwwwwww
	$handle = popen("cat /home/sargon/log/#hatcave/*.log", "r");
	$total = 0;
	$linesread = 0;
	$offset = 8; // length of channel name
	while(!feof($handle)) {
		$orig = fgets($handle);
		$linesread++;
		if($linesread % 32187 == 0) {
			print "$linesread lines read...\n"; flush();
		}
		$line = str_replace(" * ", " <", $orig);
		if($line[21 + $offset] != "<") continue;
		$who = substr($line, 22 + $offset, 3);
		$who = strtolower($who);
		$who = preg_replace("/[^a-z0-9]/", " ", $who);
		$counts[$who]++;
		if(preg_match("/$s/i", $orig)) {
			$mcounts[$who]++;
			$total++;
		}
	}
	if($total == 0) {
		print "No matches.\n";
		exit();
	}
//	print "Random quote: " . $lines[intval(mt_rand(0, count($lines)-2))] . "\n\n";

	$norm = array();
	foreach($mcounts as $k=>$v) {
		$norm[$k] = ($v / $counts[$k]) * 100;
	}

	print "First three characters of nick by line count (total $total):\n";
	pclose($handle);
	arsort($norm);
	foreach($norm as $k=>$v) {
		printf("%s: %8d of %8d lines = %7.3f%%\n", $k, $mcounts[$k], $counts[$k], $v);
	}
	$a = gettimeofday();
	$endtime = $a[sec]+$a[usec]/1000000;
	print "Generated in ";
	print $endtime-$starttime . " sec\n";
?>
