<?
	if(!isset($s)) {
		print "<form><input name=\"s\"><input type=\"submit\" value=\"Regex Search\"><br>\n";
		print "<input type=\"checkbox\" name=\"f\" value=\"1\" checked> Ignore nick, channel, timestamp, etc (prepend '(>|\* ).*' to regex)\n";
		print "</form>\n";
		print "<a href=\"http://xem.us/g/stats/stats2-lowram.php?s=&f=1\">everything</a><br>";
		print "Note: this only does #hatcave<br>";
		exit();
	}
	if($f == 1)
		$s = escapeshellarg("(>|\* ).*" . stripslashes($s));
	else
		$s = escapeshellarg(stripslashes($s));
	$a = gettimeofday();
	$starttime = $a[sec]+($a[usec]/1000000);

	set_time_limit(60 * 5); // 5 mins


	Header("Content-type: text/plain");
	print "Stats for #hatcave:\n\n";
	print "Regex is $s\n";
	$cmd = "egrep -i -h $s /home/sargon/log/#hatcave/*.log";
	print "command is $cmd\n\n";
	$handle = popen($cmd, "r");
	$linecount = 0;
	while(!feof($handle)) {
		$line = fgets($handle);
		$line = str_replace(" * ", " <", $line);
		if($line[29] != "<") continue;
		$linecount++;
//		$line = str_replace(">", "   ", $line);
//		$line = str_replace("`", "   ", $line);
//		$line = str_replace("-", "   ", $line);
//		$line = str_replace("_", "   ", $line);
		$who = substr($line, 30, 3);
		$who = strtolower($who);
		$who = preg_replace("/[^a-z0-9]/", " ", $who);
		$counts[$who]++;
	}
	if($linecount <= 0) {
		print "No matches.\n";
		exit();
	}
	print "First three characters of nick by line count (total $linecount):\n";
	arsort($counts);
	pclose($handle);
	foreach($counts as $k=>$v) {
		$p = round(($v / ($linecount - 1)) * 100, 3);
//		print "$k: $v ($p%)\n";
		printf("%s: %8d %8.3f%%\n", $k, $v, $p);
	}
	$a = gettimeofday();
	$endtime = $a[sec]+$a[usec]/1000000;
	print "Generated in ";
	print $endtime-$starttime . " sec\n";
?>
