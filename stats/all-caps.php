<?
	$s = addslashes("[^>\*](>|\* ) [A-Z][^a-z]{10,}$");
	$f = 0;

	//----------

	if($f == 1)
		$s = escapeshellarg("(>|\* ).*" . stripslashes($s));
	else
		$s = escapeshellarg(stripslashes($s));
	$a = gettimeofday();
	$starttime = $a[sec]+($a[usec]/1000000);

	set_time_limit(60 * 5); // 5 mins


	Header("Content-type: text/plain");
	print "Regex is $s\n";
	$handle = popen("egrep -h $s /home/sargon/log/#*/*.log", "r");
	unset($lines);
	while(!feof($handle)) {
		$line = fgets($handle);
		$line = str_replace(" * ", " <", $line);
		if($line[24] != "<") continue;
		$lines[] = trim($line);
//		$line = str_replace(">", "   ", $line);
//		$line = str_replace("`", "   ", $line);
//		$line = str_replace("-", "   ", $line);
//		$line = str_replace("_", "   ", $line);
		$who = substr($line, 25, 3);
		$who = strtolower($who);
		$who = preg_replace("/[^a-z0-9]/", " ", $who);
		$counts[$who]++;
	}
	if(count($lines) == 1) {
		print "No matches.\n";
		exit();
	}
	print "Random quote: " . $lines[intval(mt_rand(0, count($lines)-2))] . "\n\n";
	print "First three characters of nick by line count (total " . (count($lines) - 1) . "):\n";
	arsort($counts);
	pclose($handle);
	foreach($counts as $k=>$v) {
		$p = round(($v / (count($lines) - 1)) * 100, 3);
//		print "$k: $v ($p%)\n";
		printf("%s: %8d %8.3f%%\n", $k, $v, $p);
	}
	$a = gettimeofday();
	$endtime = $a[sec]+$a[usec]/1000000;
	print "Generated in ";
	print $endtime-$starttime . " sec\n";
?>
