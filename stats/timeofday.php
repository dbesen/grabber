<?
	set_time_limit(60 * 5); // 5 mins

	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	$nick = stripslashes($nick);
?>
<html><head>
<style><!--
table {border-collapse: collapse; }
td {border: 1px solid black; width: 70px; text-align: center;}
th {width: 70px;}
//-->
</style></head>
<body bgcolor="white">
Number of lines said on IRC by day of week and hour of day:<br>
<form>Nick:<input type="text" name="nick" value="<?=$nick?>"> Channel: <? print_select("chan", array("#*", "#ro", "#", "#hatcave", "#chatcave")); ?><input type="submit"></form><br>
<?
	require_once("../time.php");
	starttimer();
	if(!isset($chan)) $chan = "#*";
	if(!isset($nick)) $nick = "";
	$nick = "^[^<]+<" . $nick;
	$nick = escapeshellarg($nick);
	$this_year = date("Y");
	$this_month = date("m");
	$day = date("d");

	$tmp = mktime(0, 0, 0, $this_month - 1, $day, $this_year);
	$last_month = date("m", $tmp);
	$last_month_year = date("Y", $tmp);

	$times = array();
	$tot_hours = array();
	$tot_days = array();
	$total = 0;
//	$handle = popen("egrep -i -h $nick /home/sargon/log/$chan/$this_month.*.$this_year.log /home/sargon/log/$chan/$last_month.*.$last_month_year.log | sed 's/>.*/>/'", "r");
	$handle = popen("pcregrep -i -h $nick /home/sargon/log/$chan/$this_month.*.$this_year.log /home/sargon/log/$chan/$last_month.*.$last_month_year.log", "r");

	// Precalculate day-of-week table (for performance)
	$days_of_week = array();
	for($d=1;$d<31;$d++) $days_of_week[(int)$this_month][$d] = date("w", mktime(0, 0, 0, $this_month, $d, $this_year));
	for($d=1;$d<31;$d++) $days_of_week[(int)$last_month][$d] = date("w", mktime(0, 0, 0, $last_month, $d, $last_month_year));
	//
	while(!feof($handle)) {
		$line = fgets($handle);
		/* Method 1:
		if(!preg_match("/^\[(\d\d)\/(\d\d)\/(\d\d) (\d\d):/", $line, $matches)) continue;
		$month = (int) $matches[1];
		$day = (int) $matches[2];
		$year = (int) $matches[3] + 2000;
		$hour = (int) $matches[4];
		*/
		// Method 2 (faster?):
		$line = substr($line, 1); $month = (int) $line;
		$line = substr($line, 3); $day = (int) $line;
		$line = substr($line, 3); $year = (int) $line;
		$line = substr($line, 3); $hour = (int) $line;
		//
//		$day_of_week = date("w", mktime($hour, 0, 0, $month, $day, $year));
		$day_of_week = $days_of_week[$month][$day];
		$times[$day_of_week][$hour]++;
		$tot_days[$day_of_week]++;
		$tot_hours[$hour]++;
		$total++;
	}
	$max = 0;
	foreach($times as $k=>$v) {
		foreach($v as $kk=>$vv) {
			$val = $times[$k][$kk];
			if($max < $val) $max = $val;
		}
	}
	$max_day = 0;
	foreach($tot_days as $k=>$v) {
		if($max_day < $v) $max_day = $v;
	}
	$max_hour = 0;
	foreach($tot_hours as $k=>$v) {
		if($max_hour < $v) $max_hour = $v;
	}
	if($max == 0) die("Error: No chat lines found; " . getelapsed() . "s<br>");
	print "Stats for months: $last_month/$last_month_year $this_month/$this_year.  Hour is in MST.<br>\n";
	print "<table>";
	print "<tr>";
	print "<th></th>";
	print "<th>Sun</th>";
	print "<th>Mon</th>";
	print "<th>Tue</th>";
	print "<th>Wed</th>";
	print "<th>Thu</th>";
	print "<th>Fri</th>";
	print "<th>Sat</th>";
	print "<td>total</td>";
	print "</tr>";
	for($i=0;$i<24;$i++) {
		print "<tr><th>$i</th>";
		for($j=0;$j<7;$j++) {
			$val = $times[$j][$i];
			$c = get_cnum($val, $max);
			print "<td bgcolor=\"#ff{$c}{$c}\">$val</td>";
		}
		$tot_hour = $tot_hours[$i];
		$c = get_cnum($tot_hour, $max_hour);
		print "<td bgcolor=\"#ff{$c}{$c}\">$tot_hour</td>";
		print "</tr>";
	}
	print "<tr><td>total</td>";
	for($j=0;$j<7;$j++) {
		$tot_day = $tot_days[$j];
		$c = get_cnum($tot_day, $max_day);
		print "<td bgcolor=\"#ff{$c}{$c}\">$tot_day</td>";

	}
	print "<td bgcolor=\"#ff0000\">$total</td>";
	print "</tr>";
	print "</table>";
	print "Elapsed: " . getelapsed() . "s<br>";

	function get_cnum($val, $max) {
		$c = 255 - (($val * 255) / $max);
		$c = dechex($c);
		if(strlen($c) == 1) $c = "0" . $c;
		return $c;
	}

	function print_select($name, $options) {
		global $$name;
		// gets default from environment
		print "<select name=\"$name\" class=\"m\">";
		foreach($options as $option) {
			print "<option";
			if($$name == $option) print " selected";
			print ">$option</option>\n";
		}
		print "</select>";
	}
?>
</body></html>
