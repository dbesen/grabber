<?
	$LastUrl = "";
	class IrcLog {
		var $count = 0;
		var $_maxlines = 0;

		function setMaxLines($num) {
			$this->_maxlines = $num;
		}

		function get($channel, $search, $day, $justurls, $ignores) {
			global $LogPath;
			global $LastUrl;
			$this->count = 0;
			$ret = array();

			if($channel == null) $channel = '*';
			if($day == null) $day = '*';
			$day = preg_replace("/[^0-9\*\.]*/", "", $day);
			$file = "$LogPath/$channel/$day.log";
//			$file = escapeshellarg($file);

			$file = preg_replace("/\/+/", "/", $file);
			if($search != null) {
				$search = escapeshellarg($search);
//				$command = "egrep -i -h $search $file 2>/dev/null";
				// Use find if we get "Argument list too long"
//				$command = "find $LogPath/$channel/ -name '$day.log' -exec pcregrep -i -h $search {} \\;";
				$command = "pcregrep -h -i -- $search $LogPath/$channel/$day.log";
			} else {
				$command = "cat $file";
			}

//			print "command is $command<br>"; die();
			$handle = popen($command, "r");

			$done = false;
			while($done == false && !feof($handle)) {
				$line = fgets($handle);
				if(strstr($line, "#ro <X>")) continue;
				if(strlen(trim($line)) == 0) continue;
				$isUrl = highlightUrls($line, $ignores);

				// Fix spaces (since "  " displays as " " in html)
				$line = preg_replace("/  /", " &nbsp;", $line);
				$line = preg_replace("/&nbsp; /", "&nbsp;&nbsp;", $line);

				if($isUrl || ($justurls == false)) {
					$ret[]['line'] = $line;
					$ret[count($ret)-1]['n'] = $this->count; # hack for sorting
					$ret[count($ret)-1]['urls'] = $isUrl; // hack for golast
					$this->count++;
					if($this->_maxlines != 0) {
						if($this->count >= $this->_maxlines) {
							$done = true;
							$ret[]['line'] = "*** Output truncated ***\n";
							$ret[count($ret)-1]['urls'] = false;
						}
					}
				}
			}
			if($justurls == true) usort($ret, array($this, "comparerev"));
			else usort($ret, array($this, "compare"));

			if(isset($ret[0]) && is_array($ret[0]['urls'])) {
				$LastUrl = $ret[0]['urls'][count($ret[0]['urls'])-1];
			}

			$ret2 = array();
			foreach($ret as $item) {
				$r['line'] = $item['line'];
				$r['urls'] = $item['urls'];
				$ret2[] = $r;
			}
			return $ret2;
		}

		function compare($obj1, $obj2) {
			# The only thing out of place is year, so we can simply
			# prepend that and call strcmp.
			$line1 = $obj1['line'];
			$line2 = $obj2['line'];
			$line1 = substr($line1, 7, 2) . substr($line1, 0, 19);
			$line2 = substr($line2, 7, 2) . substr($line2, 0, 19);
			$val = strcmp($line1, $line2);
			if($val == 0) {
				if($obj1['n'] > $obj2['n']) return 1;
				if($obj1['n'] < $obj1['n']) return -1;
			}
			return $val;
		}

		function comparerev($line1, $line2) {
			return 0 - $this->compare($line1, $line2);
		}

		function get_channels() {
			global $LogPath;
			$ret = array("#*");
			$dirhandle = opendir($LogPath);
			while (false !== ($file = readdir($dirhandle))) {
				if(substr($file, 0, 1) == '#' && is_dir($LogPath . '/' . $file)) {
					$ret[] = $file;
				}
			}
			return $ret;
		}
	}

	function highlightUrls(&$string, $ignores) {
		// also htmlentities() the string
		// returns false if no url found, else an array of all the urls in the line
		global $regex; // set by regex.php
		$modified = false;

		$internet_explorer = false; $opera = false;
		if(strstr($GLOBALS['HTTP_USER_AGENT'], "MSIE")) $internet_explorer = true;
		if(strstr($GLOBALS['HTTP_USER_AGENT'], "Opera")) { $internet_explorer = false; $opera = true; }

		$urls = getUrls($string);
		$ret = array();

		$orig_string = $string;

		// Galen said a line with a weird character in it [04/21/10 13:54:25]
		// and this line was breaking that somehow, so we check for ""
		$string2 = htmlentities($string, null, "UTF-8");
		if($string2 !== "") $string = $string2;
		else $string = htmlentities($string);

		if((count($urls) == 0 || is_ignored($orig_string, $ignores, "line"))) return false;

		$markers = array();
		$mc = 0;
		foreach($urls as $u) {
			$scheme = $u[0];
			$url = $u[1];
			$to_remove = $scheme . $url;

			if($scheme == "") $scheme = "http://";
			$url = $scheme . $url;

			$to_remove = htmlentities($to_remove, null, "UTF-8");
			$hurl = htmlentities($url, null, "UTF-8");
			$uurl = urlencode($url); // bad?
			//print("hurl is $hurl, uurl is $uurl, to_remove is $to_remove<br>\n");
			if(!is_ignored($url, $ignores, "url")) {
				$ret[] = $url;
//						$words[$k] = str_replace($repl, "<a href=\"javascript:a('$url');\">$repl</a>$trailing", $word);
				// Make sure the marker isn't in the string
				do {
					$m = "URL_$mc";
					$mc++;
				} while(strpos($string, $m) !== false);
				$markers[$m] = $to_remove;
				$string = str_replace_one($to_remove, $m, $string);
				//if($internet_explorer)
				//	$markers[$m] = "<a href=\"$hurl\" onClick=\"return f();\" onMouseUp=\"a('$uurl')\">$to_remove</a>";
				//else if($opera)
				//	$markers[$m] = "<a href=\"$hurl\">$to_remove</a>";
				//else
					//$markers[$m] = "<a href=\"$hurl\">$to_remove</a>";
					//$markers[$m] = "<a href=\"strip-referrer.php?u=$uurl\">$to_remove</a>";
					// todo: chrome, re-test ie, re-test opera
					//$markers[$m] = "<a href=\"$hurl\" onClick=\"this.href='strip-referrer.php?u=$uurl'\">$to_remove</a>";
					$markers[$m] = "<a href=\"$hurl\" onMouseOut=\"gb(this, '$url');\" onMouseMove=\"gb(this, '$url');\" onMouseDown=\"gb(this, '$url');\" onMouseUp=\"g(event, this, '$uurl');\">$to_remove</a>";
				$modified = true;
			}
		}
		// Now that all the urls are replaced with markers, we go through and replace them back with the appropriate urls.
		// This is so a later replacement doesn't match an earlier one.
		foreach($markers as $k=>$v) {
				$string = str_replace_one($k, $v, $string);
		}

		if($modified == true) return $ret;
		return false;
	}

	$cached_line_ignores = null;
	$cached_url_ignores = null;
	$cached_word_ignores = null;
	$ignorecount = 0;
	function is_ignored($string, $doIgnores, $FullStr) {
		global $LineIgnoresPath;
		global $UrlIgnoresPath;
		global $WordIgnoresPath;
		global $cached_line_ignores;
		global $cached_url_ignores;
		global $cached_word_ignores;
		global $ignorecount;
		if($doIgnores == false) return false;
		switch($FullStr) {
		case "line":
			if($cached_line_ignores == null) $cached_line_ignores = file($LineIgnoresPath);
			$ignores = $cached_line_ignores;
			break;
		case "url":
			if($cached_url_ignores == null) $cached_url_ignores = file($UrlIgnoresPath);
			$ignores = $cached_url_ignores;
			break;
		}
		
		foreach($ignores as $asdf) {
			$asdf = trim($asdf);
			if(strlen($asdf) > 1 && $asdf[0] == '/') {
				if(preg_match($asdf, $string)) {
					$ignorecount++;
					return true;
				}
			}
		}
		return false;
	}

	function str_replace_one($search, $replace, $content){
		if ($pos = strpos($content, $search)) {
			return substr($content, 0, $pos) . $replace . substr($content, $pos+strlen($search));
		} else {
			return $content;
		}
	}

?>
