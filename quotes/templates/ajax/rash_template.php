<?php
/*
RQMS - Rash Quote Management System
Copyright (C) 2003-2004 Tom Cuchta (tommah@instable.net / http://www.mastergoat.com) and Instable Network (p00p@instable.net / http://www.instable.net)

http://rqms.sourceforge.net

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// This is a basic color shift of the bash_template (bash clone), but it uses more technology and advanced things
// Rather than straight up tables and HTML 4.01, this template uses more CSS and XHTML 1.0
// This template may cause older browsers to malfunction and such since it uses newer technologies, blabla

function template(){
global $section;
switch ($section){
	case 0:
		$section='Administration';
		break;
	case 1:
		$section='Home';
		break;
	case 2:
		$section='Latest';
		break;
	case 3:
		$section='Browse';
		break;
	case 4:
		$section='Random';
		break;
	case 5:
		$section='Top 150';
		break;
	case 6:
		$section='Bottom';
		break;
	case 7:
		$section='Add';
		break;
	case 8:
		$section='Search';
		break;
	case 9:
		$section='Quotes of the Week';
		break;
	case 10:
		$section='Upgrade Quote';
		break;
	case 11:
		$section='Downgrade Quote';
		break;
}
global $rashversion;

echo "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n"
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>Rash Quote Management System: <?=$section?></title>
<style type="text/css">
<!--
	@import url("./templates/rash_template/rash_style.css");
-->
</style>
<meta name="robots" content="noarchive,nofollow" />
<script type="text/javascript">
var _=_?_:{}
_.ajax=_.X=function(u,f,d,x){x=window.ActiveXObject;x=new(x?x:XMLHttpRequest)('Microsoft.XMLHTTP');x.open(d?'POST':'GET',u,1);x.setRequestHeader('Content-type','application/x-www-form-urlencoded');x.onreadystatechange=function(){x.readyState>3&&f?f(x.responseText,x):0};x.send(d)}
_.id=_.G=function(e){return e.style?e:_.d.getElementById(e)}
_.d=document

function plus(ID){
  _.ajax("?ratingplus&id="+ID, function(){
    _.G("rating"+ID).innerHTML = parseInt(_.G("rating"+ID).innerHTML)+1
  })
}
function minus(ID){
  _.ajax("?ratingminus&id="+ID, function(){
    _.G("rating"+ID).innerHTML = parseInt(_.G("rating"+ID).innerHTML)-1
  })
}
</script>
</head>
<body>
	<form method="post" action="?quotesearch">
		<table class="bars_table" cellspacing="0">
			<tr>
				<td class="blue_bar">
            		<span id="qms">QMS</span>&nbsp; 
	    	        <a href="./?admin" id="admin_link">Admin</a>&nbsp;
<?
if(login_check(0)){
	echo "				<a href=\"./?logout\" id=\"log_out\">Log Out</a>\n";
}
?>
				</td>
				<td class="blue_bar">
            		<div id="section"><?=$section?></div>
				</td>
			</tr>
			<tr>
				<td class="grey_bar" colspan="2">
					<a href="./">Home</a>&nbsp;/
					<a href="./?latest">Latest</a>&nbsp;/
					<a href="./?browse">Browse</a>&nbsp;/
					<a href="./?random">Random</a>
					<a href="./?random2">&gt;0</a>&nbsp;/
					<a href="./?top150">Top 150</a>&nbsp;/
					<a href="./?bottom">Bottom</a>&nbsp;/
					<a href="./?add"><span style="font-weight:bold">Add Quote</span></a>&nbsp;/
					<a href="./?qotw">QotW</a>&nbsp;/
					<a href="./?search">Search</a>&nbsp;<span class="grey_hash">/&nbsp;#</span><input type="text" name="qSEARCH" size="4" class="grey_input" />
				</td>
			</tr>
		</table>
	</form>
	<div id="content_format">
<?content();// output of the rash index is inserted here?>
	</div>
	<form method="post" action="?quotesearch">
		<table class="bars_table" cellspacing="0">
			<tr>
				<td class="grey_bar" colspan="2">
					<a href="./">Home</a>&nbsp;/
					<a href="./?latest">Latest</a>&nbsp;/
					<a href="./?browse">Browse</a>&nbsp;/
					<a href="./?random">Random</a>
					<a href="./?random2">&gt;0</a>&nbsp;/
					<a href="./?top150">Top 150</a>&nbsp;/
					<a href="./?bottom">Bottom</a>&nbsp;/
					<a href="./?add"><span style="font-weight:bold">Add Quote</span></a>&nbsp;/
					<a href="./?qotw">QotW</a>&nbsp;/
					<a href="./?search">Search</a>&nbsp;/&nbsp;#<input type="text" name="qSEARCH" size="4" class="grey_input" />
				</td>
			</tr>
			<tr>
				<td class="blue_bar">
					<div id="rash_version">Rash Version: <?=$rashversion?>&nbsp;</div>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
<? } ?>
