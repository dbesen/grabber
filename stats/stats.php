<?
	$a = gettimeofday();
	$starttime = $a[sec]+($a[usec]/1000000);

	Header("Content-type: text/plain");
	print "First three characters of nick by line count:\n";
	$handle = popen("cat /home/sargon/log/#*/*.log", "r");
	while(!feof($handle)) {
		$line = fgets($handle);
		$line = str_replace(" * ", " <", $line);
		if($line[24] != "<") continue;
//		$line = str_replace(">", "   ", $line);
//		$line = str_replace("`", "   ", $line);
//		$line = str_replace("-", "   ", $line);
//		$line = str_replace("_", "   ", $line);
		$who = substr($line, 25, 3);
		$who = strtolower($who);
		$who = preg_replace("/[^a-z0-9]/", " ", $who);
		$counts[$who]++;
	}
	arsort($counts);
	pclose($handle);
	foreach($counts as $k=>$v) {
		print "$k: $v\n";
	}
	$a = gettimeofday();
	$endtime = $a[sec]+$a[usec]/1000000;
	print "Generated in ";
	print $endtime-$starttime . " sec\n";
?>
