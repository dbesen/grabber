<?
// This needs... on first page, list of nicks with count of misspelled, and overall list of misppelled words
// when you click on a nick, you get that user's list of words..
// when youclick on a word, you get a list of nicks that misspelled that word

	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");


	$pspell_conf_link = pspell_config_create("en", "american");
//	pspell_config_ignore($pspell_conf_link, 4);
	pspell_config_mode($pspell_conf_link, PSPELL_FAST);
	pspell_config_runtogether($pspell_conf_link, true);
	$pspell_link = pspell_new_config($pspell_conf_link);


	$month = date("m");
	$year = date("Y");

	// We have to do this since the versions of pspell and aspell on feh aren't correct
	// so pspell_save_wordlist doesn't work
	$ignores = file("spell-ignores.txt");
	$my_ignores = array();
	foreach($ignores as $k=>$v) {
		$v = trim($v);
		if(!preg_match("/^'|'$/", $v))
			pspell_add_to_session($pspell_link, $v);
		else
			$my_ignores[] = $v;
	}

	$h = popen("fgrep -h '<' /home/sargon/log/#ro/*.$month.$year.log", "r");

	$count = 0;
	while(!feof($h)) {
		$row = fgets($h);
		if($row[24] != '<') continue;
//		if(!strstr($row, "sarg")) continue;
		$nick = substr($row, 25, 3);
		$nick = strtolower($nick);
		$nick = preg_replace("/[^a-z].*/", "", $nick);
		$nick = trim($nick);
		if($nick == "c") continue;
		$row = preg_replace("/^[^>]+>/", "", $row);
		$row = preg_replace("/[^a-z']/i", " ", $row);
		$row = trim($row);
		$words = preg_split("/ +/", $row);
		foreach($words as $currword) {
			if(checkSpelling($currword)) continue;
//			if(in_array($currword, $ignores)) continue;
			if(in_array($currword, $my_ignores)) continue;
			$nickcounts[$nick]++;
			$wordcounts[$currword]++;
			$wrongnick[$nick][$currword]++;
			$wrongword[$currword][$nick]++;
		}
	}

	if(isset($name)) {
		$name = stripslashes($name);
		print "Nick $name:<br>\n";
		$words = $wrongnick[$name];
		arsort($words);
		foreach($words as $k=>$v) {
			$n = urlencode($k);
			print "<a href=\"?word=$n\">$k</a>: $v<br>\n";
		}
		exit();
	}

	if(isset($word)) {
		$word = stripslashes($word);
		print "Word $word:<br>\n";
		$nicks = $wrongword[$word];
		arsort($nicks);
		foreach($nicks as $k=>$v) {
			$n = urlencode($k);
			print "<a href=\"?name=$n\">$k</a>: $v<br>\n";
		}
		exit();
	}
	print "Misspellings for the current month ($month) (<a href=\"spell-ignores.txt\">ignores</a>):<br>";
	arsort($nickcounts);
	arsort($wordcounts);

	$count = count($wordcounts);
	print "<table border=\"1\"><tr><th>Nicks</th><th>Words ($count)</th></tr><tr><td valign=\"top\">";
	foreach($nickcounts as $k=>$v) {
		$n = urlencode($k);
		print "<a href=\"?name=$n\">$k</a>: $v<br>\n";
	}

	print "</td><td>";

	foreach($wordcounts as $k=>$v) {
		$n = urlencode($k);
		print "<a href=\"?word=$n\">$k</a>: $v<br>\n";
	}

	print "</tr></table>";
//	print "<pre>"; print_r($wrong); print "</pre>";
//	print "<pre>"; print_r($wrongnick); print "</pre>";
	exit();

	function checkSpelling($word) {
		global $pspell_link;
		if(pspell_check($pspell_link, $word)) return true;
		return false;
	}
?>
