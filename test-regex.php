<?

// TODO: change all add_test lines to add_test

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
error_reporting(E_ALL);

print "<pre>";

require_once("regex.php");

$tests = array();

add_test("www.google.com", array(array("", "www.google.com")));
add_test("google.com", array(array("", "google.com")));
add_test("google.com/", array(array("", "google.com/")));
add_test("http://www.url.com/path/file.html?query=string&asdf=jkl", array(array("http://", "www.url.com/path/file.html?query=string&asdf=jkl")));
add_test("http://1.2.3.4/path/file.html?query=string&asdf=jkl", array(array("http://", "1.2.3.4/path/file.html?query=string&asdf=jkl")));
add_test("4.2.2.1", array());
add_test("http://4.2.2.1", array(array("http://", "4.2.2.1")));
add_test("127.0.0.1", array());
add_test("www.google.com,", array(array("", "www.google.com")));
add_test("www.google.com/,", array(array("", "www.google.com/")));
add_test("<www.google.com/>,", array(array("", "www.google.com/")));
add_test("'google.com'", array(array("", "google.com")));
add_test("'google.com/'", array(array("", "google.com/")));
add_test("'url.com/path/file.html?query=string'", array(array("", "url.com/path/file.html?query=string")));
add_test("\"www.google.com\"", array(array("", "www.google.com")));
add_test("\"www.google.com,\"", array(array("", "www.google.com")));
add_test("<www.google.com>", array(array("", "www.google.com")));
add_test("http://www.url.com/?args=a,b", array(array("http://", "www.url.com/?args=a,b")));
add_test("google.com!", array(array("", "google.com")));
add_test("google.com/!", array(array("", "google.com/!")));
add_test("(www.google.com/),", array(array("", "www.google.com/")));
add_test("(www.google.com/)", array(array("", "www.google.com/")));
add_test("(google.com/)", array(array("", "google.com/")));
add_test("(google.com)", array(array("", "google.com")));
add_test("(http://google.com)", array(array("http://", "google.com")));
add_test("url.com?", array(array("", "url.com")));
add_test("url.com/?", array(array("", "url.com/")));
add_test("url.com/file.html?", array(array("", "url.com/file.html")));
add_test("'url.com/?'", array(array("", "url.com/")));
add_test("url.com/?jkl", array(array("", "url.com/?jkl")));
add_test("url.com?asdf=jkl", array(array("", "url.com")));
add_test("url.com/?asdf=jkl", array(array("", "url.com/?asdf=jkl")));
add_test("url.com/?12345", array(array("", "url.com/?12345")));
add_test("toshiba.co.jp", array(array("", "toshiba.co.jp")));
add_test("asdf", array());
// add_test("exeem://asdf", "exeem://asdf");
add_test("ftp://user:pass@url.domain.com/path/file.txt", array(array("ftp://", "user:pass@url.domain.com/path/file.txt")));
add_test("https://url.com", array(array("https://", "url.com")));
add_test("http://url.com:81/path/file?query=string", array(array("http://", "url.com:81/path/file?query=string")));
add_test("http://", array());
add_test("asdf.bat", array());
add_test("asdf.scr", array());
add_test("-asdf.com", array(array("", "asdf.com")));
add_test("http://-asdf.com", array(array("", "asdf.com")));
add_test("asdf-.com", array());
add_test("asdf-jkl.com", array(array("", "asdf-jkl.com")));
add_test('func("http://google.com");', array(array("http://", "google.com")));
add_test("http://google.com/?arg=1234..", array(array("http://", "google.com/?arg=1234")));
add_test("http://en.wikipedia.org/wiki/Aspect_ratio_(image)", array(array("http://", "en.wikipedia.org/wiki/Aspect_ratio_(image)")));
add_test("asdf.pl", array());
add_test("http://asdf.pl", array(array("http://", "asdf.pl")));
add_test("asdf.sh", array());
add_test("http://asdf.sh", array(array("http://", "asdf.sh")));
add_test("asdf.so", array());
add_test("http://asdf.so", array(array("http://", "asdf.so")));
add_test("asdf.id", array());
add_test("http://asdf.id", array(array("http://", "asdf.id")));
add_test("asdf.ve", array());
add_test("http://asdf.ve", array(array("http://", "asdf.ve")));
add_test("asdf.in", array());
add_test("http://asdf.in", array(array("http://", "asdf.in")));
add_test("asdf.ms", array());
add_test("http://asdf.ms", array(array("http://", "asdf.ms")));
add_test("asdf.py", array());
add_test("http://asdf.py", array(array("http://", "asdf.py")));
add_test("asdf.do", array());
add_test("http://asdf.do", array(array("http://", "asdf.do")));
add_test("25.25.25.25", array());
add_test("http://25.25.25.25", array(array("http://", "25.25.25.25")));
add_test("email@domain.com", array());
add_test("\"(asdf.com)\"", array(array("", "asdf.com")));
add_test("\"(asdf.com/)\"", array(array("", "asdf.com/")));
add_test("\"asdf (asdf.com/)\"", array(array("", "asdf.com/")));
add_test("\"asdf.com)\"", array(array("", "asdf.com")));
add_test("\"asdf.com/asdf_)\"", array(array("", "asdf.com/asdf_)")));
add_test("coolstuff.museum/foo", array(array("", "coolstuff.museum/foo")));
add_test("coolstuff.faketld/foo", array());
add_test("asdf (asdf google.com/)", array(array("", "google.com/")));
add_test("asdf (asdf google.com/) asdf", array(array("", "google.com/")));
add_test("asdf (google.com/ asdf) asdf", array(array("", "google.com/")));
//add_test("asdf (google.com/ google.com/) asdf", array(array("", "google.com/"), array("", "google.com/"))); // evil.
add_test("asdf (asdf google.com/)\t", array(array("", "google.com/")));
add_test("asdf \"(google.com/\")", array(array("", "google.com/")));
add_test("asdf (\"google.com/)\")", array(array("", "google.com/")));
add_test("hello google.com/.", array(array("", "google.com/")));
add_test("ashttp://google.com", array(array("http://", "google.com")));
add_test(":o @ meebo.com", array(array("", "meebo.com")));
add_test("http://uncyclopedia.org/wiki/Te:", array(array("http://", "uncyclopedia.org/wiki/Te:")));
add_test('http://cheston.com/pbf/archive.html\\\\\\\\\\', array(array('http://', 'cheston.com/pbf/archive.html')));
add_test('http:\\\\g.xem.us\\test-regex.php', array(array('http:\\\\', 'g.xem.us\\test-regex.php'))); // believe it or not
add_test("http://google.com.abc", array(array("http://", "google.com")));
add_test("java.io.IOException", array());
add_test("javascript:openNASAWindow('http://google.com/')", array(array("http://", "google.com/")));
add_test("http://asdf.mobi", array(array("http://", "asdf.mobi")));
add_test('<a href="some .cn site">www.eharmony.com/confirm_the_account</a>', array(array("", "www.eharmony.com/confirm_the_account")));
add_test("http://grouphug.us/confessions/172589529%20and%20id%20=É", array(array("http://", "grouphug.us/confessions/172589529%20and%20id%20=É")));
add_test("http://utah.travel/", array(array("http://", "utah.travel/")));
add_test("mms://media.eharmony.com/stream/NBC-TonightShow.wmv", array(array("mms://", "media.eharmony.com/stream/NBC-TonightShow.wmv")));
add_test("http://labs.ideeinc.com/multicolour/#colors=d3c8fc,501000,789b40;", array(array("http://", "labs.ideeinc.com/multicolour/#colors=d3c8fc,501000,789b40;")));
add_test("http://pichaus.com/worst-tattoo-ever-8yv@", array(array("http://", "pichaus.com/worst-tattoo-ever-8yv@")));
add_test("http://guantes.f3h.com/rightnicks.png", array(array("http://", "guantes.f3h.com/rightnicks.png")));
add_test("gopher://thegrebs.com/0/awesome.txt", array(array("gopher://", "thegrebs.com/0/awesome.txt")));
add_test("Last.fm", array(array("", "Last.fm")));

add_test("http://www.google.com", array(array("http://", "www.google.com")));
add_test("google.com", array(array("", "google.com")));
add_test("google.com google.com", array(array("", "google.com"), array("", "google.com")));
add_test("google.com http://google.com", array(array("", "google.com"), array("http://", "google.com")));
add_test("http://google.com http://google.com", array(array("http://", "google.com"), array("http://", "google.com")));
add_test("http://google.com google.com", array(array("http://", "google.com"), array("", "google.com")));
add_test("http://google.com ftp://google.com", array(array("http://", "google.com"), array("ftp://", "google.com")));
add_test("http:google.com", array(array("", "google.com")));
add_test("http://news.yahoo.com/nphotos/Republican-presidential-nominee-shaking-hands-Senator-Barack-Obama-presidential-debate/photo//081016/ids_photos_ts/r1772410910.jpg/;_ylt=AujAY6EXHmMQy5Zsha09SbsDW7oF", array(array("http://", "news.yahoo.com/nphotos/Republican-presidential-nominee-shaking-hands-Senator-Barack-Obama-presidential-debate/photo//081016/ids_photos_ts/r1772410910.jpg/;_ylt=AujAY6EXHmMQy5Zsha09SbsDW7oF")));
add_test("[04/01/06 07:51:51] #hatcave <sargon> my favorite photo.net pic of all time is still http://photo.net/photodb/photo?photo_id=128895", array(array("", "photo.net"), array("http://", "photo.net/photodb/photo?photo_id=128895")));
add_test("http://minus-zero.net./images/logic1100.gif", array(array("http://", "minus-zero.net./images/logic1100.gif")));
add_test("http://www.ecofont.eu/look_at_ecofont_en.html", array(array("http://", "www.ecofont.eu/look_at_ecofont_en.html")));
add_test("out of curiosity, does this work from the command line: php -r \"print file_get_contents('http://www.google.com/robots.txt');\"", array(array("http://", "www.google.com/robots.txt")));
add_test("golast is g.xem.us click on first link, or g.xem.us/golast.php", array(array("", "g.xem.us"), array("", "g.xem.us/golast.php")));
add_test("invalid IRC message 'Warning: file_get_contents(http://www.fmylife.com/random): failed to open stream: Connection refused in /home/bishop/rbot2/lib/rubybot2/bin/fml.php on line 5'", array(array("http://", "www.fmylife.com/random")));
add_test("<Gand-w3rk> hah, i just realized www.twitter.com -> twitter.com", array(array("", "www.twitter.com"), array("", "twitter.com")));
add_test("!remember borderlands save editor == http://blog.gib.me/2009/10/31/borderlands-save-editor-revision-10/", array(array("http://", "blog.gib.me/2009/10/31/borderlands-save-editor-revision-10/")));
add_test("http://☃.net", array(array("http://", "☃.net")));
add_test("∞.com", array(array("", "∞.com")));
add_test("<bascule> ∞.com", array(array("", "∞.com")));
add_test("☻.com", array(array("", "☻.com")));
add_test(" ☻.com", array(array("", "☻.com")));
add_test("> ☻.com", array(array("", "☻.com")));
add_test("<bascule> ☻.com", array(array("", "☻.com")));
add_test("♛.com", array(array("", "♛.com")));
add_test("<bascule> google.com", array(array("", "google.com")));
add_test('http:\\\\google.com', array(array('http:\\\\', "google.com")));
// add_test("I get taken for granted.It's happening", array());
add_test("http://videocafe.crooksandliars.com/heather/snl-palin-2012-disaster-movie-parody?ref=http://videocafe.crooksandliars.com/heather/snl-palin-2012-disaster-movie-parody", array(array("http://", "videocafe.crooksandliars.com/heather/snl-palin-2012-disaster-movie-parody?ref=http://videocafe.crooksandliars.com/heather/snl-palin-2012-disaster-movie-parody")));
add_test("http://en.wikipedia.org/wiki/Downtown_no_Gaki_no_Tsukai_ya_Arahende!!", array(array("http://", "en.wikipedia.org/wiki/Downtown_no_Gaki_no_Tsukai_ya_Arahende!!")));
add_test("<bascule> !fight site:hp.com linux vs site:hp.com hp-ux", array(array('', 'hp.com'), array('', 'hp.com')));
add_test("http://to/", array(array("http://", "to/")));
add_test("http://to", array(array("http://", "to")));
add_test("http://to.", array(array("http://", "to"))); // the . is a valid part of the url here, but it also works without it, and . is a term_char
add_test("http://to./", array(array("http://", "to./")));
add_test("to", array());
add_test("to.", array());
add_test("now that's how to win FHF ---> http://www.facebook.com/photo.php?pid=30688035&id=1412589748", array(array("http://", "www.facebook.com/photo.php?pid=30688035&id=1412589748")));
add_test("[04/21/10 13:54:25] #hatcave <galen> stairs are soo passé - http://www.aww-kittah-aww.com/up/public/147625/15x1wu05472104.gif", array(array("http://", "www.aww-kittah-aww.com/up/public/147625/15x1wu05472104.gif")));
// add_test("http://www.google.comhttp://www.google.com", array(array("http://", "www.google.com"), array("http://", "www.google.com")));
add_test("eztv.it", array(array("", "eztv.it")));
add_test("will.i.am", array());
add_test("http://will.i.am", array(array("http://", "will.i.am")));
add_test("Will.I.Am", array());
add_test("http://Will.I.Am", array(array("http://", "Will.I.Am")));
add_test("http://kitty.cat/", array(array("http://", "kitty.cat/")));
add_test("http://rim.jobs/", array(array("http://", "rim.jobs/")));


run_tests();

print "</pre>";

function add_test($str, $result) {
	global $tests;
	$item['str'] = $str;
	$item['result'] = $result;
	$tests[] = $item;
}

function run_tests() {
	global $tests;
	$failed = 0;
	foreach($tests as $v) {
		print "Testing: '<font color=\"gray\">" . htmlentities($v['str']) . "</font>': ";
		$result = getUrls($v['str']);
		if($result !== $v['result']) {
			print "<font color=\"red\">Test failed</font>: expected '" . htmlentities(serialize($v['result'])) . "', got '<font color=\"red\">" . htmlentities(serialize($result)) . "</font>'\n";
			$failed++;
		}
		else print "Success ('<font color=\"green\">" . htmlentities(serialize($result)) . "</font>')\n";
	}
	print "\n";
	if($failed == 0) print "Full string result: Success\n";
	else print "<font color=\"red\">Full string result: $failed tests failed</font>\n";
}

?>
