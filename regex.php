<?

$countries = "(?:a[cdefgilmnoqrstuwz]|"
. "b[abdefghijmnorstvwyz]|"
. "c[acdfghiklmnoruvxyz]|"
. "d[ejkmoz]|"
. "e[ceghrstu]|"
. "f[ijkmor]|"
. "g[adefghilmnpqrstuwy]|"
. "h[kmnrtu]|"
. "i[delmnoqrst]|"
. "j[emop]|"
. "k[eghimnprwyz]|"
. "l[abcikrstuvy]|"
. "m[acdeghklmnopqrstuvwxyz]|"
. "n[acefgilopruz]|"
. "om|"
. "p[aefghklmnrstwy]|"
. "qa|"
. "r[eouw]|"
. "s[abcdeghijklmnortuvyz]|"
. "t[cdfghjkmnoprtvwz]|"
. "u[agkmsyz]|"
. "v[aceginu]|"
. "w[fs]|"
. "y[etu]|"
. "z[amw])";

$term_chars = '\'\"\`,\(\?\.\\\\'; # appear in url but not at end of url, so "word url", etc works..  see also strip_surround calls

$domain_chars = '[a-z0-9\x{0080}-\x{FFFF}]'; // used to be "[a-z0-9]", but that doesn't work for unicode

$regex = '/'
. '((?:https?|ftp|mms|gopher):(?:\/\/|\\\\\\\\))?' # scheme.. 8 backslashes become 2 in final string
. '(' # start of url group
. '(?:(?:[a-z0-9\-]+\:)?[a-z0-9\-]+\@)?' # user and pass
. '(?:(?:(?:(?!0\.)\d{1,3}\.){3}(?!0)[0-9]{1,3})|' # dotted ip
. '(?:(?:'
. '(?:' . $domain_chars . '(?:\-?' . $domain_chars . ')*\.)+' # domain (no hyphen at start or end)
. '(com|net|org|mil|gov|edu|biz|museum|aero|coop|info|pro|name|int|mobi|travel|cat|jobs|' . $countries . '))' # tld, no ?: since we store url in \3
. ')|(?<=http:\/\/)to)' # hack for http://to
. '(?:\:\d+)?' # port
. '\.?' # a dot is actually legal here
. '(?:' # group of path and query string, since we can't have a query string without a path
. '(?:(?:\/|\\\\)[^\s\?]*)' # path
. '(?:\?[^\s]*)?' # query string
. ')?' # end query string/path group
. '(?:(?<![\s' . $term_chars . ']))' // term_char hack
// . '|(?:(?<=exeem:\/\/)\S+)' # hack for exeem.. can't replace the \S here with '.' to match spaces because the thing that calls this splits on space
// . '|(?:(?<=http:\/\/)[^\s\/]+\/\S+)' # hack for matching http://machine/share/ urls.. uncomment to match them
. ')' # end of url group
. '(\.[a-z]+)?' # word.word.word false positive -- go into matches[4]
. '(?:\s|\b|$|[' . $term_chars . '])+' # end of url
. '/iu';

	function getUrls($string) {
		// returns array(array(scheme, url), array(scheme, url), ...)
		// Doesn't return duplicate items
		global $regex;
		$urls = array();

		if(!((strstr($string, ".") || strstr($string, "://")) && preg_match($regex, $string))) return array();

		// split on spaces which follow words that have a dot in them
		$w = explode(" ", $string);
		$words = array();
		$str = array();
		foreach($w as $word) {
			$str[] = $word;
			if(strstr($word, ".")) {
				$words[] = implode(" ", $str);
				$str = array();
			}
		}
		$words[] = implode(" ", $str);

		foreach($words as $k=>$word) {
			if(do_match($word, $matches)) {
				$scheme = $matches[1];
				$url = $matches[2];
				$to_add = array($scheme, $url);
				//if(!in_array($to_add, $urls))
					$urls[] = $to_add;
			}
		}

		return $urls;
	}

	// this expects input to be split on spaces which follow words that have a dot in them, so (asdf google.com/) works
	function do_match($word, &$matches = null) { // returns bool for whether or not it matched
		if($word == "") return false;
		$matches = null; // array(scheme, url including tld, tld)
		global $regex;
		global $term_chars;

		$changed = true;
		while($changed) { // this is so order + count doesn't matter for the surround chars
			$changed = false;
			$changed |= strip_regex($word, '/<\/a>/i'); // common false positive...
			$changed |= strip_surround($word, "\"");
			$changed |= strip_surround($word, "'");
			$changed |= strip_surround($word, "(", ");");
			$changed |= strip_surround($word, "(", "):");
			$changed |= strip_surround($word, "(", ")");
			$changed |= strip_surround($word, "<", ">");
			$changed |= strip_regex($word, "/[$term_chars]+$/i");
		}

		$res = preg_match($regex, $word, $mat);
		if($res == false) return false;

		$scheme = $mat[1];
		$url = $mat[2];
		if(isset($mat[3])) $tld = $mat[3]; else $tld = "";

		$tld = strtolower($tld);

		if($scheme == "") {
			if(isset($mat[4])) return false;
			if($tld == "pl") return false;
			if($tld == "sh") return false;
			if($tld == "so") return false;
			if($tld == "id") return false;
			if($tld == "ve") return false;
			if($tld == "ro") return false;
			if($tld == "in") return false;
			if($tld == "ms") return false;
			if($tld == "do") return false;
			if($tld == "py") return false;
			//if($tld == "it") return false;
			if($tld == "") return false; // dotted ips
		}

		// One special case that happens a lot
		if($scheme == "" && strtolower($url) == "will.i.am") return false;

		// so email addresses don't match
		if(strstr($url, "@") && ($scheme == "") && (!strstr($url, "/"))) {
			return false;
		}
		unset($mat[4]);
		$matches = $mat;
		return true;
	}

	function strip_surround(&$word, $before, $after = null) {
		$word = trim($word);
		if($after == null) $after = $before;
		global $regex;
		if(!strstr($word, $before)) return false; // faster than the preg_match..
		// find the start of the url
		if(preg_match($regex, $word, $mat, PREG_OFFSET_CAPTURE)) {
			$start = $mat[0][1];
			$pre = substr($word, 0, $start);
			$post = substr($word, 0 - strlen($after));
//			print "word is $word\n";
//			print "strlen is " . strlen($word) . "\n";
//			print "pre is $pre\n";
			if($post == $after && strstr($pre, $before)) {
				$word = substr($word, 0, strlen($word)-(strlen($after)));
				return true;
			}
		} else {
			return false;
		}
	}

	function strip_regex(&$word, $regex) {
		$word = preg_replace($regex, "", trim($word), -1, $c);
		if($c >= 1) return true;
		return false;
	}
?>
