<?
	date_default_timezone_set("America/denver");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	error_reporting(E_ALL);

	require_once("regex.php");
	require_once("irc.php");

	$home = $_SERVER['DOCUMENT_ROOT'];
	$LineIgnoresPath = "$home/ignores/line-ignores.txt";
	$UrlIgnoresPath = "$home/ignores/url-ignores.txt";
	$WordIgnoresPath = "$home/ignores/word-ignores.txt";

	// ------------- settings
	$LogPath = "/home/sargon/log/";
	$MaxLines = 8000;
	// -----------------

	$log = new IrcLog;
	if(!isset($channel)) $channel = "#*";
	if(!isset($search)) $search = null;
	$search = stripslashes($search);
	$justurls = "Just Urls";
//	if(!isset($dr)) $dr = 1;
	if(!isset($day)) $day = date("d");
	if(!isset($month)) $month = date("m");
	if(!isset($year)) $year = date("Y");
	if(isset($_COOKIE['grabber_ignores'])) $ignores = false;
	else $ignores = true;

	if($justurls == "Just Urls") $justurls = true; else $justurls = false;
//	if($dr == 2) $date = "*";
//	else $date = "$month.$day.$year";
	$LastUrl = "";
	while($LastUrl == "") {

		$date = "$month.$day.$year";
		$log->setMaxLines($MaxLines);
		$log->get($channel, $search, $date, $justurls, $ignores);

		// subtract 1 from date
		$yesterday = mktime(0, 0, 0, $month, $day - 1, $year);
		$month = date("m", $yesterday);
		$day = date("d", $yesterday);
		$year = date("Y", $yesterday);
	}
	if(preg_match("/^http:\/\/(www\.)?xem\.us\/g\/golast\.php/", $LastUrl)) {
		print "golast loop detected, url is $LastUrl";
		exit();
	}
	$LastUrl = urlencode($LastUrl);
	header("Location: strip-referrer.php?u=$LastUrl");

?>
