<?
	$a = gettimeofday();
	$starttime = $a[sec]+($a[usec]/1000000);

	Header("Content-type: text/plain");
	print "Hook usage (ignoring hooks with only 1 hit):\n";
	$handle = popen("cat /home/sargon/log/#*/*.log", "r");
	if($handle === false) {
		die("Error opening pipe");
	}
	while(!feof($handle)) {
		$line = fgets($handle);
		if(preg_match("/^\[[^]]+][^<]+<[^>]+> (\![^\!\_\@\?\# ]\S+)/", $line, $matches)) {
			$command = $matches[1];
			$cmdcounts[$command]++;
			$line = str_replace(" * ", " <", $line);
			if($line[24] != "<") continue;
//		$line = str_replace(">", "   ", $line);
//		$line = str_replace("`", "   ", $line);
//		$line = str_replace("-", "   ", $line);
//		$line = str_replace("_", "   ", $line);
			$who = substr($line, 25, 3);
			$who = strtolower($who);
			$who = preg_replace("/[^a-z0-9]/", " ", $who);
			$counts[$who]++;
		}
	}
	arsort($counts);
	arsort($cmdcounts);
	pclose($handle);
	foreach($cmdcounts as $k=>$v) {
		if($v != 1)
			print "$k: $v\n";
	}
	print "\nPeople who use hooks:\n";
	foreach($counts as $k=>$v) {
		print "$k: $v\n";
	}
	$a = gettimeofday();
	$endtime = $a[sec]+$a[usec]/1000000;
	print "Generated in ";
	print $endtime-$starttime . " sec\n";
?>
