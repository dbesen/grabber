<?
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	

	function checkPassword($user, $pass) {

		$file = file("inc/passwd");
		$file = explode(":", $file[0]);
		if($user != trim($file[0])) return false;
		if($pass != trim($file[1])) return false;

		return true;
	}
	
	function doAuth() {
		global $PHP_AUTH_USER; // for backwards compatibility
		if(isset($_POST['logout'])) {
			Logout();
			exit();
		}
		if(isset($_POST['user'])) {
			$expire = null;
			if(isset($_POST['remember']) && $_POST['remember'] == 1) {
				$expire = time() + 60*60*24*365; // remember for 1 year (we have a logout button)
			}
			if(setcookie("user", $_POST['user'], $expire, "/") == false) {
				print "Error setting cookie.";
				exit();
			}
			setcookie("pass", md5($_POST['pass']), $expire, "/");
			
			
			header("Location: {$_SERVER['REQUEST_URI']}");
			exit();
		}

		if(isset($_COOKIE['user']) && isset($_COOKIE['pass']) && checkPassword($_COOKIE['user'], $_COOKIE['pass']) == true) {
			$PHP_AUTH_USER = $_COOKIE['user'];
			// renew the cookie here?
			return;
		} else {
			if(headers_sent()) {
				setcookie("user", null, null, "/");
				setcookie("pass", null, null, "/");
				die("Unable to authenticate -- headers sent before doAuth()");
			}
			print "<html><body onload=\"document.forms.auth.user.focus();\">";
			print "<form method=\"POST\" name=\"auth\">";

			if(true) {	# Use this line to disable new calendar and payroll logins
				$can_remember = true;
				print "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" height=\"100%\">";
				print "<tr><td valign=\"center\" align=\"center\">";
				print "<div style=\"font-size: 24\">Urlgrabber</div>";
				print "<div style=\"font-size: 18\">ask sargon</div>";
				print "<br />";
				print "<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\" style=\"border: 1px solid black;\">";
				print "<tr><th align=\"right\">";
				print "Login: ";
				print "</th><td>";
				print "<input type=\"text\" name=\"user\"><br />\n";
				print "</td></tr><tr><th align=\"right\">";
				print "Password: ";
				print "</th><td>";
				print "<input type=\"password\" name=\"pass\"><br />\n";
				print "</td></tr><tr><td colspan=\"2\">";
				print "<table width=\"100%\"cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tr><td>";
				if($can_remember) {
					print "<input type=\"checkbox\" name=\"remember\" value=\"1\"> Auto-login";
				}
				print "</td><td align=\"right\">";
				print "<input type=\"submit\" value=\"Submit\">";
				print "</td></tr></table>";
				print "</td></tr></table></td></tr></table>";
			} else {
				print "<input name='auth' type='hidden' />";
				print "System maintenance is being performed.  Please check back.";
			}
			print "</form></body></html>";
			exit();
		}
	}

	function Logout() {
		setcookie("user", null, null, "/");
		setcookie("pass", null, null, "/");
		header("Location: {$_SERVER['REQUEST_URI']}");
		exit();
	}

	doAuth();
?>
